import numpy as np
import matplotlib.colors as col
import pandas as pd
import math
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colorbar
from matplotlib.colors import ListedColormap
from datetime import datetime

#data from dataframe
# read CSV file from my computer's downloads folder
data_set = pd.read_csv("/home/chris/Downloads/multi_aoi_analysis_rawframe_2018_12_03.csv.tar.gz",parse_dates=['valid_time', 'analysis_time'],
infer_datetime_format=True)

#define a variable that contains a specific model from the dataframe
df = data_set.loc[data_set.model == "COAMPS-SOCAL_SOCAL-n3-a1"]
print len(data_set)

#define a variable that contains a specific parameter
df = df.loc[df.parameter == "air_temp"]
print len (df)

#define a variable that contains a specific analysis_time
df = df.loc[df.analysis_time == '2018-08-20 00:00:00']
print len (df)

#define a variable that contains a specific valid_time
df = df.loc[df.valid_time >= '2018-08-20 00:00:00']
times = df.valid_time

#define a variable that contains a specific elevation
df = df.loc[df.elevation == 2.0]
print len (df)

#yaxis
times = df.valid_time

#xaxis
dataset = ["metars_specis","ship_synoptic","surface_synoptic","buoy observations"]

#all  valid_times
utimes = np.unique(times)

#array size
data =np.empty([17,4])

i = 0

#for loop to iterate each unique verified mae
for id in utimes:
  print id
  #filter on time
  newdf = df.loc[df['valid_time']==id]
  print np.array(newdf.verified_mae)
  data[i, :] = np.array(newdf['verified_mae'])
  i = i + 1

#Get rid of excess digits in verified mae scores
data = np.around(data, decimals=2)

#Plot Data
fig, ax = plt.subplots()
im = ax.imshow(data)

#Title of visualization
ax.set_title("Air Temperature Recorded on August 20th SOCAL-n3-a1 Model")

# We want to show all ticks...
ax.set_xticks(np.arange(len(dataset)))
ax.set_yticks(np.arange(len(utimes)))

# ... and label them with the respective list entries
ax.set_xticklabels(dataset)
ax.set_yticklabels(np.datetime_as_string(utimes, unit='s'))

#label y axis
plt.ylabel("Valid Times")

# Rotate the tick labels and set their alignment.
plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
         rotation_mode="anchor")

# Loop over data dimensions and create text annotations.
for i in range(len(utimes)):
    for j in range(len(dataset)):
        text = ax.text(j, i, data[i, j],
                       ha="center", va="center", color="w")

# Create colorbar
cbar = ax.figure.colorbar(im, ax=ax)
cbarlabel=("verified_MaE")
cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom",)

#Label colorbar
cbar.ax.set_ylabel('Verified MAE/Valid Time', rotation=270)